from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):

    GENDER_CHOICES = [
        ('Fe', 'Female'),
        ('Ma', 'Male'),
    ]

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dob = models.DateField(auto_now=False, auto_now_add=False)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=50)
    grade = models.CharField(max_length=20)
    profile_pic = models.ImageField(upload_to='profile_pics', blank=True)

    def __str__(self):
        return self.user.username