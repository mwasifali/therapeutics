from django.shortcuts import render, get_object_or_404, redirect
from users.forms import UserForm, UserProfileForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .models import UserProfile

# Create your views here.
def index(request):
    return render(request, 'users/index.html')

def settings(request, id):
    profile_record = get_object_or_404(UserProfile, id=id)
    form = UserProfileForm(request.POST or None, instance=profile_record)
    
    user_record = get_object_or_404(User, id=profile_record.user_id)
    user_form = UserForm(request.POST or None, instance=user_record)

    if form.is_valid() and user_form.is_valid():
        form.save()
        user_form.save()
        return redirect("../../../")
    else:
        form = UserProfileForm(instance=profile_record)   
        user_form = UserForm(instance=user_record)    
    context = {'form': form, 'user_form': user_form} 
    return render(request, 'users/settings.html', context)

def sub_level(request):
    # my_record = UserProfile.objects.get()
    return render(request, 'users/sub_level.html')

def level_question(request):
    return render(request, 'users/level_questions.html')

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

@login_required
def special(request):
    return HttpResponse("You are logged in !")  

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user_form.cleaned_data['password'])
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'profile_pic' in request.FILES:
                print('found it')
                profile.profile_pic = request.FILES['profile_pic']
            profile.save()
            registered = True
        else:
            print(user_form.errors,profile_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()
    return render(request,'users/registration.html',
                          {'user_form':user_form,
                           'profile_form':profile_form,
                           'registered':registered})

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        print(user)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            return HttpResponse("Invalid login details given")
    else:
        return render(request, 'users/login.html', {})