from django.contrib import admin
from django.urls import path, include
from users import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index, name='index'),
    path('special/',views.special,name='special'),
    path('users/',include('users.urls')),
    path('logout/', views.user_logout, name='logout'),
    path('users/<int:id>/my_profile/', views.settings, name='settings'),
    path('sub_level/', views.sub_level, name='sub_level'),
    path('level_questions/', views.level_question, name='level_question'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)